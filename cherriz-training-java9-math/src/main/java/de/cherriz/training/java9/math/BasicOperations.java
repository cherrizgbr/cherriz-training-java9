package de.cherriz.training.java9.math;

public class BasicOperations {

    public static int add(int a, int b) {
        return a + b;
    }

    public static int sub(int a, int b) {
        return a - b;
    }

    public static int div(int a, int b) {
        return a / b;
    }

    public static int mult(int a, int b) {
        return a * b;
    }

}
