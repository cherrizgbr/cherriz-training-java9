package de.cherriz.training.java9.react.publisher;

import java.util.concurrent.Flow;

@FunctionalInterface
public interface RequestError {

    void error(Flow.Subscriber<? super Integer> subscriber);

}