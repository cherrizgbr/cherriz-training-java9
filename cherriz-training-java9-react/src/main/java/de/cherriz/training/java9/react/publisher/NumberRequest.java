package de.cherriz.training.java9.react.publisher;

import java.util.concurrent.Flow;

@FunctionalInterface
public interface NumberRequest {

    void request(Flow.Subscriber<? super Integer> subscriber, long n);

}