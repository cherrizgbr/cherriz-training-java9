package de.cherriz.training.java9.react;

import de.cherriz.training.java9.react.publisher.NumberPublisher;
import de.cherriz.training.java9.react.subscriber.NumberSubscriber;

import java.util.concurrent.Flow;

public class Starter {

    public static void main(String[] args) {
        Flow.Publisher<Integer> publisher = new NumberPublisher(8);
        Flow.Subscriber<Integer> subscriberA = new NumberSubscriber("A", 3);
        Flow.Subscriber<Integer> subscriberB = new NumberSubscriber("B", 3);

        publisher.subscribe(subscriberA);
        publisher.subscribe(subscriberB);
    }

}
