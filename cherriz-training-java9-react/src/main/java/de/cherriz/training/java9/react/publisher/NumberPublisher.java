package de.cherriz.training.java9.react.publisher;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.Flow.Publisher;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.currentThread;

public class NumberPublisher implements Publisher<Integer> {

    private final ExecutorService executor = Executors.newFixedThreadPool(4);

    private final AtomicInteger value = new AtomicInteger();

    private final AtomicBoolean finished = new AtomicBoolean();

    private final int elements;

    public NumberPublisher(int elements) {
        this.elements = elements;
    }

    @Override
    public void subscribe(Subscriber<? super Integer> subscriber) {
        NumberSubscription subscription = new NumberSubscription(subscriber, this::publishItems, this::publisError);
        subscriber.onSubscribe(subscription);
    }

    private void publishItems(Flow.Subscriber<? super Integer> subscriber, long n) {
        for (int i = 0; i < n; i++) {
            executor.execute(() -> {
                if (value.get() == elements) {
                    finished.set(true);
                    return;
                }
                int v = value.incrementAndGet();

                System.out.printf("Publisher:\t\t[%s]\tPublishing item: %s\n", currentThread().getName(), v);
                subscriber.onNext(v);
            });
            if (finished.get()) {
                executor.shutdown();
                break;
            }
        }
    }

    private void publisError(Flow.Subscriber<? super Integer> subscriber) {
        executor.execute(() -> subscriber.onError(new IllegalArgumentException()));
    }

}