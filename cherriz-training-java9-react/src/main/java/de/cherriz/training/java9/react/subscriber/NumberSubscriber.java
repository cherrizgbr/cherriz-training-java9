package de.cherriz.training.java9.react.subscriber;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

import static java.lang.Thread.currentThread;

public class NumberSubscriber implements Subscriber<Integer> {

    private int count = 0;

    private final int blocksize;

    private String name;

    private Subscription subscription;

    public NumberSubscriber(String name, int blocksize) {
        this.name = name;
        this.blocksize = blocksize;
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;

        System.out.printf("Subscriber %s:\t[%s]\tSubscribed\n", this.name, currentThread().getName());

        requestItems();
    }

    private void requestItems() {
        System.out.printf("Subscriber %s:\t[%s]\tRequesting %s new Items\n", this.name, currentThread().getName(), this.blocksize);
        this.count = 0;
        this.subscription.request(this.blocksize);
    }

    @Override
    public void onNext(Integer item) {
        System.out.printf("Subscriber %s:\t[%s]\tReceived item %s\n", this.name, currentThread().getName(), item);

        this.count++;
        if (this.count == this.blocksize) {
            requestItems();
        }
    }

    @Override
    public void onComplete() {
        System.out.printf("Subscriber %s:\t[%s]\tComplete\n", this.name, currentThread().getName());
    }

    @Override
    public void onError(Throwable t) {
        System.out.printf("Subscriber %s:\t[%s]\t%s\n", this.name, currentThread().getName(), t);
    }

}