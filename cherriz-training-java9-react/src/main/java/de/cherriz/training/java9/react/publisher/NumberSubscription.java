package de.cherriz.training.java9.react.publisher;

import java.util.concurrent.Flow;
import java.util.concurrent.atomic.AtomicBoolean;


public class NumberSubscription implements Flow.Subscription {

    private AtomicBoolean isCanceled = new AtomicBoolean(false);

    private Flow.Subscriber<? super Integer> subscriber;

    private final NumberRequest request;

    private final RequestError error;

    NumberSubscription(Flow.Subscriber<? super Integer> subscriber, NumberRequest request, RequestError error) {
        this.subscriber = subscriber;
        this.request = request;
        this.error = error;
    }

    @Override
    public void request(long n) {
        if (isCanceled.get()) {
            return;
        }

        if (n < 0) {
            error.error(subscriber);
        } else {
            request.request(subscriber, n);
        }
    }

    @Override
    public void cancel() {
        isCanceled.set(true);
    }

}