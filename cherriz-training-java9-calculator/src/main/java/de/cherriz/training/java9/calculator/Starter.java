package de.cherriz.training.java9.calculator;

public class Starter {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[2]);

        char opp = args[1].charAt(0);

        Calculator calc = new Calculator();
        calc.add(a);
        switch (opp) {
            case '+':
                calc.add(b);
                break;
            case '-':
                calc.substract(b);
                break;
            case '*':
                calc.multiply(b);
                break;
            case '/':
                calc.divide(b);
                break;
            case '%':
                calc.mod(b);
                break;
        }
        System.out.format("%d %s %d = %d", a, opp, b, calc.getCurrent());
    }

}
