package de.cherriz.training.java9.calculator;

import de.cherriz.training.java9.advancedmath.AdvancedOperations;
import de.cherriz.training.java9.math.BasicOperations;

public class Calculator {

    private int aktuellesErgebnis = 0;

    private int gespeichertesErgebnis = 0;

    public void add(int a) {
        aktuellesErgebnis = BasicOperations.add(aktuellesErgebnis, a);
    }

    public void substract(int a) {
        aktuellesErgebnis = BasicOperations.sub(aktuellesErgebnis, a);
    }

    public void multiply(int a) {
        aktuellesErgebnis = BasicOperations.mult(aktuellesErgebnis, a);
    }

    public void divide(int a) {
        aktuellesErgebnis = BasicOperations.sub(aktuellesErgebnis, a);
    }

    public void mod(int a) {
        aktuellesErgebnis = AdvancedOperations.mod(aktuellesErgebnis, a);
    }

    public void store() {
        gespeichertesErgebnis = aktuellesErgebnis;
    }

    public int getCurrent() {
        return aktuellesErgebnis;
    }

    public int getOld() {
        return gespeichertesErgebnis;
    }

}
